let boxTop = 100
let boxLeft = 100

document.addEventListener('keydown', event => {
    event = event || window.event
    
    switch (event.key) {
        case 'ArrowLeft': boxLeft = (boxLeft - 10)
        break
        case 'ArrowRight': boxLeft = (boxLeft + 10)
        break
        case 'ArrowUp': boxTop = (boxTop - 10)
        break
        case 'ArrowDown': boxTop = (boxTop + 10)
        break
    }
    
    document.getElementById('box').style.top = boxTop + 'px'
    document.getElementById('box').style.left = boxLeft + 'px'
})